/* src/UDPReader.cpp
 * This contains all the function definitions for the UDP Reader class
 */
#include "UDPReader.hpp"

#include <iostream>
#include <sys/socket.h>
#include <netinet/in.h>
#include <unistd.h>

#include <algorithm>

#include "SMS_UDP_Definitions.hpp"
#include "processing.hpp"

UDPReader::UDPReader(int port)
    : mPort(port), processor()
{
    // Create a place to store the incoming data
    mBuffer = new char[SMS_UDP_MAX_PACKETSIZE];

    mSocketFD = socket(AF_INET, SOCK_DGRAM, 0);
    if (mSocketFD < 0)
    {
        std::cerr << "Error creating socket with error code " << errno << std::endl;
        exit(-1);
    }

    mReceiveAdress.sin_family = AF_INET;
    mReceiveAdress.sin_port = htons(mPort);
    mReceiveAdress.sin_addr.s_addr = htonl(INADDR_ANY);

    int succes_code;
    succes_code = bind(mSocketFD, (sockaddr*)&mReceiveAdress, sizeof(sockaddr_in));
    if (succes_code < 0)
    {
        std::cerr << "Error binding to port with error code " << errno << std::endl;
        exit(-1);
    }
}

UDPReader::~UDPReader()
{
    delete[] mBuffer;
}

void UDPReader::ReceivePacket()
{
    // Read packet
    std::fill(&mBuffer[0], &mBuffer[SMS_UDP_MAX_PACKETSIZE-1], (char)0); // Fill the buffer with zeros
    mLastBytesRead = recvfrom(mSocketFD, (void *)mBuffer, SMS_UDP_MAX_PACKETSIZE, 0, nullptr, nullptr);
    if (mLastBytesRead < 0)
    {
        std::cerr << "Warning, could not receive packet! errno: " << errno << std::endl;
        return;
    }
}

void UDPReader::HandleData()
{
    // Handle the incoming data

    PacketBase base;
    base = *reinterpret_cast<PacketBase*>(mBuffer);
    processor.setData(mBuffer, base.mPacketType);

    processor.OnReceiveNewPacket();
}

void UDPReader::Listen()
{
    while (1)
    {
        ReceivePacket();
        HandleData();
    }
}
