/* src/main.cpp
 * Entry point of the UDP listener here the port is configured and the listener is started
 */

#define SMS_UDP_PORT 5606

#include "UDPReader.hpp"

int main(int argc, char* argv[])
{
    UDPReader pc2reader(SMS_UDP_PORT);
    pc2reader.Listen();
}
