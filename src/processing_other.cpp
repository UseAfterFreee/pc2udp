#include "processing.hpp"

#include <algorithm>

#include "SMS_UDP_Definitions.hpp"

DataProcessor::DataProcessor()
{
    // initialize all data with zeroes
    std::fill_n((char*)&dataTelemetry,               sizeof(sTelemetryData),               (char)0);
    std::fill_n((char*)&dataRace,                    sizeof(sRaceData),                    (char)0);
    std::fill_n((char*)&dataParticipants,            sizeof(sParticipantsData),            (char)0);
    std::fill_n((char*)&dataTimings,                 sizeof(sTimingsData),                 (char)0);
    std::fill_n((char*)&dataGameState,               sizeof(sGameStateData),               (char)0);
    std::fill_n((char*)&dataTimeStats,               sizeof(sTimeStatsData),               (char)0);
    std::fill_n((char*)&dataParticipantVehicleNames, sizeof(sParticipantVehicleNamesData), (char)0);
    std::fill_n((char*)&dataVehicleClassNames,       sizeof(sVehicleClassNamesData),       (char)0);
}

void DataProcessor::setData(void *data, int type)
{
    lastPacketType = type;

    switch (type)
    {
        case eCarPhysics:
            std::copy(reinterpret_cast<char*>(data), reinterpret_cast<char*>(data)+sizeof(sTelemetryData),    (char*)&dataTelemetry);
            break;
        case eRaceDefinition:
            std::copy(reinterpret_cast<char*>(data), reinterpret_cast<char*>(data)+sizeof(sRaceData),         (char*)&dataRace);
            break;
        case eParticipants:
            std::copy(reinterpret_cast<char*>(data), reinterpret_cast<char*>(data)+sizeof(sParticipantsData), (char*)&dataParticipants);
            break;
        case eTimings:
            std::copy(reinterpret_cast<char*>(data), reinterpret_cast<char*>(data)+sizeof(sTimingsData),      (char*)&dataTimings);
            break;
        case eGameState:
            std::copy(reinterpret_cast<char*>(data), reinterpret_cast<char*>(data)+sizeof(sGameStateData),    (char*)&dataGameState);
            break;
        case eTimeStats:
            std::copy(reinterpret_cast<char*>(data), reinterpret_cast<char*>(data)+sizeof(sTimeStatsData),    (char*)&dataTimeStats);
            break;
        case eParticipantVehicleNames:
            // TODO
            break;
    }
}
