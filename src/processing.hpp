/* src/processing.hpp
 * Contains the functions that the reader will call when processing packets
 */

/* Notes on packet types
 *
 * eCarPhysics -> sTelemetryData
 * eRaceDefinition -> sRaceData
 * eParticipants -> sParticipantsData
 * eTimings -> sTimingsData
 * eGameState -> sGameStateData
 * eWeatherState -> NOT SENT
 * eVehicleNames -> NOT SENT
 * eTimeStats -> sTimeStatsData
 * eParticipantVehicleNames -> sParticipantVehicleNamesData or sVehicleClassNamesData
 */
#ifndef PROCESSING_HPP_
#define PROCESSING_HPP_

#include "SMS_UDP_Definitions.hpp"

class DataProcessor
{
 private:
     sTelemetryData                 dataTelemetry;
     sRaceData                      dataRace;
     sParticipantsData              dataParticipants;
     sTimingsData                   dataTimings;
     sGameStateData                 dataGameState;
     // eWeatherState               -> NOT SENT
     // eVehicleNames               -> NOT SENT
     sTimeStatsData                 dataTimeStats;
     // At this point the only way to know which of these following
     // two was sent is by looking at the package header, it can't be
     // found in lastPacketType
     sParticipantVehicleNamesData   dataParticipantVehicleNames;
     sVehicleClassNamesData         dataVehicleClassNames;

     int                            lastPacketType;  // EUDPStreamerPacketHanlderType
 public:
     DataProcessor();
     void OnReceiveNewPacket();

     void setData(void *data, int type);
};

#endif  // PROCESSING_HPP_
