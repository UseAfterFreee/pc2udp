/* src/processing.cpp
 * Here functions can be defined that get called when a specific packet is received
 */
#include "SMS_UDP_Definitions.hpp"
#include "processing.hpp"

#include <iostream>

void DataProcessor::OnReceiveNewPacket()
{
    if (lastPacketType == eCarPhysics)
    {
        std::cout << "\nPacket " << dataTelemetry.sBase.mPacketNumber << std::endl;
        std::cout << "sSpeed: " << dataTelemetry.sSpeed << std::endl;
        std::cout << "sOilTempCelsius: " << dataTelemetry.sOilTempCelsius << std::endl;
        std::cout << "sThrottle: " << static_cast<unsigned int>(dataTelemetry.sThrottle) << std::endl;
        std::cout << "sSteering: " << static_cast<int>(dataTelemetry.sSteering) << std::endl;
    }
}
