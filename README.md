# Project cars 2 UDP packet listener

A UDP client that reads and interprets the packets from Project Cars.
Everything is written in c++ and can be compiled on linux systems using the included makefile and g++.

## How it works
First the packets are read using a linux UDP socket. Then the data in the packets is converted to the relevant struct in the [src/SMS\_UDP\_Definitions.hpp](https://gitlab.com/UseAfterFreee/pc2udp/blob/master/src/SMS_UDP_Definitions.hpp).
These structs filled with the data are then saved in the DataProcessor class declared in [src/processing.hpp](https://gitlab.com/UseAfterFreee/pc2udp/blob/master/src/processing.hpp).
In this class the data can be processed and shown to the user or passed on to another device, this is achieved by defining the OnReceiveNewPacket function.
Examples of these functions are available in [src/processing.cpp](https://gitlab.com/UseAfterFreee/pc2udp/blob/master/src/processing.cpp).

## Why it was created
This was created in order to assist a friend of mine who wanted read and process the packets using a raspberry pi and then use I²C to send this processed data to an arduino.
The arduino then would display the data using an LED Matrix. For more information on this project his fork can be checked out [here](http://www.gitlab.com/kingma1999/PC2UDP).

## Building from source
In order to build this from source, (for example on a Raspberry Pi or other linux powered machine) you need the following programs.
 * g++         (The GNU Compiler Collection c++ compiler)
 * make        (The program that will make sure the call the compiler with the correct parameters)
 * git or wget (To download the source code, in case of wget you will also need tar)

The steps are listed here
1. First download the source code
    * Using wget:
    ```
    wget https://gitlab.com/UseAfterFreee/pc2udp/-/archive/Protocol2_V1.0/pc2udp-Protocol2_V1.0.tar.gz
    tar -xzvf pc2udp-Protocol2_V1.0.tar.gz
    cd pc2udp-Protocol2_V1.0
    ```
    * Using git:
    ```
    git clone https://gitlab.com/UseAfterFreee/pc2udp.git
    cd pc2udp
    (optional switch to tag) git checkout Protocol2_V1.0
    ```
2. Building the sources
    * Without debugging info (recommended)
    ```
    make
    ```
    * With debugging info
    ```
    make debug
    ```
3. Running
    * After succesfully building it the bin/ folder should contain the executable to run the program
